{
  "schema_version": "1.0.0",
  "elements": [
    {
      "description": "# cells-radio-tabs\n\n![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)\n![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)\n\n[Demo of component in Cells Catalog](https://catalogs.platform.bbva.com/cells)\n\n`<cells-radio-tabs>` displays a list of tabs from its `options` property.\nThe `options` property can be an array of strings or an array of objects with an optional `icon` property and a `label` property that will be used as the tab text.\n\nExample with simple options:\n\n```html\n<cells-radio-tabs options='[\"Home\", \"Accounts\"]'></cells-radio-tabs>\n```\n\nExample with text and icons:\n\n```html\n<cells-radio-tabs options='[{\n  \"icon\": \"coronita:home\",\n  \"label\": \"Home\"\n}, {\n  \"icon\": \"coronita:alarm\",\n  \"label\": \"Alerts\"\n}]'></cells-radio-tabs>\n```\n\n## Icons\n\nSince this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.\n\n## Styling\n\nThe following custom properties and mixins are available for styling:\n\n| Custom Property | Description | Default |\n| :-------------- | :---------- | :------ |\n| --cells-radio-tabs-border-radius | border-radius applied to host | 0 |\n| --cells-radio-tabs-color | Text color | var(--bbva-300, #D3D3D3) |\n| --cells-radio-tabs-item-height | Tab height | 3.4375rem (55px) |\n| --cells-radio-tabs-bg-color | background-color applied to host | var(--bbva-white, #fff) |\n| --cells-radio-tabs | Mixin applied to :host | {} |\n| --cells-radio-tabs-option-flex | `flex` property applied to each tab | 1 |\n| --cells-radio-tabs-border-bottom-color | border-bottom color for each tab | var(--bbva-200, #E9E9E9) |\n| --cells-radio-tabs-option | Empty mixin for each tab | {} |\n| --cells-radio-tabs-color-selected | Text color for selected tab | var(--bbva-core-blue, #004481) |\n| --cells-radio-tabs-option-focus | Empty mixin applied to focused tabs | {} |\n| --cells-radio-tabs-tab-hover | Empty mixin applied to tab on :hover | {} |\n| --cells-radio-tabs-option-total | Empty mixin for tab total span | {} |\n| --cells-radio-tabs-selected | Empty mixin for selected tab | {} |\n| --cells-radio-tabs-content | Empty mixin for tab content (icon + text) | {} |\n| --cells-radio-tabs-icon-margin | Margin applied to icon | 0 0.625rem 0 0 (0 10px 0 0) |\n| --cells-radio-tabs-icon | Empty mixin for the icon | {} |\n| --cells-radio-tabs-indicator-bg-color | background-color for the current tab indicator | var(--bbva-core-blue, #004481) |",
      "summary": "Display a list of navigation tabs.",
      "path": "cells-radio-tabs/cells-radio-tabs.js",
      "properties": [
        {
          "name": "options",
          "type": "Array",
          "description": "List of options.\nCan be an array of Strings used as labels for each tab or Objects with a \"label\" key and and optional \"icon\" key to display\nan icon to the left of the text.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 84,
              "column": 6
            },
            "end": {
              "line": 90,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "observer": "\"_optionsChanged\"",
              "attributeType": "Array"
            }
          },
          "defaultValue": "[]"
        },
        {
          "name": "iconSize",
          "type": "number | null | undefined",
          "description": "Size for the icons.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 95,
              "column": 6
            },
            "end": {
              "line": 98,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "attributeType": "Number"
            }
          },
          "defaultValue": "24"
        },
        {
          "name": "selected",
          "type": "number | null | undefined",
          "description": "Index of the selected option.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 103,
              "column": 6
            },
            "end": {
              "line": 107,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "notify": true,
              "attributeType": "Number"
            }
          },
          "defaultValue": "0"
        },
        {
          "name": "_uniqueID",
          "type": "number | null | undefined",
          "description": "",
          "privacy": "protected",
          "sourceRange": {
            "start": {
              "line": 109,
              "column": 6
            },
            "end": {
              "line": 114,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "attributeType": "Number"
            }
          }
        },
        {
          "name": "_hasLabels",
          "type": "boolean | null | undefined",
          "description": "",
          "privacy": "protected",
          "sourceRange": {
            "start": {
              "line": 116,
              "column": 6
            },
            "end": {
              "line": 119,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "readOnly": true,
              "attributeType": "Boolean"
            }
          }
        },
        {
          "name": "notifyMouseEvents",
          "type": "boolean | null | undefined",
          "description": "Set to true to fire `tab-mouseevent` event when a tab receives mouseenter / mouseleave.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 124,
              "column": 6
            },
            "end": {
              "line": 127,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "attributeType": "Boolean"
            }
          },
          "defaultValue": "false"
        }
      ],
      "methods": [
        {
          "name": "_optionsChanged",
          "description": "",
          "privacy": "protected",
          "sourceRange": {
            "start": {
              "line": 137,
              "column": 2
            },
            "end": {
              "line": 143,
              "column": 3
            }
          },
          "metadata": {},
          "params": [
            {
              "name": "newValue"
            },
            {
              "name": "previousValue"
            }
          ],
          "return": {
            "type": "void"
          }
        },
        {
          "name": "_updateTabStylesAfterResettingSelected",
          "description": "Prevents changing the width and the position of the indicator at the same time.",
          "privacy": "protected",
          "sourceRange": {
            "start": {
              "line": 146,
              "column": 2
            },
            "end": {
              "line": 153,
              "column": 3
            }
          },
          "metadata": {},
          "params": [],
          "return": {
            "type": "void"
          }
        },
        {
          "name": "_resetSelected",
          "description": "",
          "privacy": "protected",
          "sourceRange": {
            "start": {
              "line": 155,
              "column": 2
            },
            "end": {
              "line": 164,
              "column": 3
            }
          },
          "metadata": {},
          "params": []
        },
        {
          "name": "_setTabStyles",
          "description": "",
          "privacy": "protected",
          "sourceRange": {
            "start": {
              "line": 166,
              "column": 2
            },
            "end": {
              "line": 180,
              "column": 3
            }
          },
          "metadata": {},
          "params": [],
          "return": {
            "type": "void"
          }
        },
        {
          "name": "_setSelected",
          "description": "Selects the item on click event and on keydown only if the key pressed is space or enter.\nPrevents selecting an item while navigating through tabs using the tab key.",
          "privacy": "protected",
          "sourceRange": {
            "start": {
              "line": 186,
              "column": 2
            },
            "end": {
              "line": 200,
              "column": 3
            }
          },
          "metadata": {},
          "params": [
            {
              "name": "e"
            }
          ],
          "return": {
            "type": "void"
          }
        },
        {
          "name": "_computeChecked",
          "description": "",
          "privacy": "protected",
          "sourceRange": {
            "start": {
              "line": 202,
              "column": 2
            },
            "end": {
              "line": 204,
              "column": 3
            }
          },
          "metadata": {},
          "params": [
            {
              "name": "selected"
            },
            {
              "name": "index"
            }
          ]
        },
        {
          "name": "_selectedChanged",
          "description": "",
          "privacy": "protected",
          "sourceRange": {
            "start": {
              "line": 206,
              "column": 2
            },
            "end": {
              "line": 211,
              "column": 3
            }
          },
          "metadata": {},
          "params": [
            {
              "name": "selected"
            },
            {
              "name": "options"
            }
          ],
          "return": {
            "type": "void"
          }
        },
        {
          "name": "_computeHasLabels",
          "description": "",
          "privacy": "protected",
          "sourceRange": {
            "start": {
              "line": 213,
              "column": 2
            },
            "end": {
              "line": 215,
              "column": 3
            }
          },
          "metadata": {},
          "params": [
            {
              "name": "options"
            }
          ]
        },
        {
          "name": "_onMouseEvent",
          "description": "",
          "privacy": "protected",
          "sourceRange": {
            "start": {
              "line": 217,
              "column": 2
            },
            "end": {
              "line": 234,
              "column": 3
            }
          },
          "metadata": {},
          "params": [
            {
              "name": "e"
            }
          ],
          "return": {
            "type": "void"
          }
        }
      ],
      "staticMethods": [],
      "demos": [
        {
          "url": "demo/index.html",
          "description": ""
        }
      ],
      "metadata": {},
      "sourceRange": {
        "start": {
          "line": 70,
          "column": 0
        },
        "end": {
          "line": 235,
          "column": 1
        }
      },
      "privacy": "public",
      "superclass": "Polymer.Element",
      "name": "CellsRadioTabs",
      "attributes": [
        {
          "name": "options",
          "description": "List of options.\nCan be an array of Strings used as labels for each tab or Objects with a \"label\" key and and optional \"icon\" key to display\nan icon to the left of the text.",
          "sourceRange": {
            "start": {
              "line": 84,
              "column": 6
            },
            "end": {
              "line": 90,
              "column": 7
            }
          },
          "metadata": {},
          "type": "Array"
        },
        {
          "name": "icon-size",
          "description": "Size for the icons.",
          "sourceRange": {
            "start": {
              "line": 95,
              "column": 6
            },
            "end": {
              "line": 98,
              "column": 7
            }
          },
          "metadata": {},
          "type": "number | null | undefined"
        },
        {
          "name": "selected",
          "description": "Index of the selected option.",
          "sourceRange": {
            "start": {
              "line": 103,
              "column": 6
            },
            "end": {
              "line": 107,
              "column": 7
            }
          },
          "metadata": {},
          "type": "number | null | undefined"
        },
        {
          "name": "notify-mouse-events",
          "description": "Set to true to fire `tab-mouseevent` event when a tab receives mouseenter / mouseleave.",
          "sourceRange": {
            "start": {
              "line": 124,
              "column": 6
            },
            "end": {
              "line": 127,
              "column": 7
            }
          },
          "metadata": {},
          "type": "boolean | null | undefined"
        }
      ],
      "events": [
        {
          "type": "CustomEvent",
          "name": "cells-radio-tabs-styles-updated",
          "description": "Fired after updating the tab styles.",
          "metadata": {}
        },
        {
          "type": "CustomEvent",
          "name": "selected-tab",
          "description": "Fired when tab was selected",
          "metadata": {}
        },
        {
          "type": "CustomEvent",
          "name": "tab-mouseevent",
          "description": "Fired on tab mouseenter / mouseleave",
          "metadata": {}
        },
        {
          "type": "CustomEvent",
          "name": "selected-changed",
          "description": "Fired when the `selected` property changes.",
          "metadata": {}
        }
      ],
      "styling": {
        "cssVariables": [],
        "selectors": []
      },
      "slots": [],
      "tagname": "cells-radio-tabs"
    }
  ]
}
