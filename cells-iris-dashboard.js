{
  const {
    html,
  } = Polymer;
  /**
    `<cells-iris-dashboard>` Description.

    Example:

    ```html
    <cells-iris-dashboard></cells-iris-dashboard>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-iris-dashboard | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsIrisDashboard extends Polymer.Element {

    static get is() {
      return 'cells-iris-dashboard';
    }

    static get properties() {
      return {
        p1: {
          type: Boolean,
          value: false
          },
          Alarray:{
            type: Object,
            value: {},
            notify: true
            
          }
      };
    }
    showTemp(e){
      if(e.detail.value === true){
        this.set('p1',e.detail.value);
      }
    
    }
    filtrado(nombre) {
      if (!nombre) {
        return null;
      }
      else {
        nombre = nombre.toLowerCase();
        return function(Alarray) {
          var first = Alarray.name.toLowerCase();
          return (first.indexOf(nombre) != -1 );
        };
      }
    }

    static get template() {
      return html `
      <style include="cells-iris-dashboard-styles cells-iris-dashboard-shared-styles"></style>
      
        <div class="search">
          <strong>Filtrar por nombre: </strong>
          <input type="text" name="nombre" placeholder="Escribe un nombre" value="{{nombre::input}}">
        </div>
        <cells-mocks-component alumnos="{{Alarray}}"></cells-mocks-component>
        <template is="dom-repeat" items="{{Alarray}}" filter="{{filtrado(nombre)}}">
          <div class="card card2 ">
            <div >
              <img src="{{item.img}}">
            </div>
            <p>{{item.name}} {{item.last}}</p>
            <p><b>Dirección:</b>{{item.address}}</p>
            <p><b>Hobbies:</b>{{item.hobbies}}</p>
          </div>
        </template>

        <div class="card">
          <iron-ajax
          auto
          url="https://api.chucknorris.io/jokes/random"
          handle-as="json"
          last-response="{{data}}"></iron-ajax>
          <p>chucknorris dice:</p>
          <div class="chuck"><img  src="{{data.icon_url}}"></div>
          <p>{{data.value}}</p>
         </div>

      `;
    }
  }

  customElements.define(CellsIrisDashboard.is, CellsIrisDashboard);
}